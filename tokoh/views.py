from django.http.response import HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import render
from django.db import connection
from collections import namedtuple


# Function to return every row of data from query
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def ini_tokoh(request):
    cursor = connection.cursor()
    result = []
    try:
        cursor.execute("SET SEARCH_PATH TO THECIMS")
        cursor.execute("SELECT username_pengguna, nama, jenis_kelamin, status, xp, energi, kelaparan, hubungan_sosial, level FROM TOKOH")
        result= namedtuplefetchall(cursor)
    except Exception as e:
        print(e)
    finally:
        cursor.close()

    return render(request, 'tokoh.html', {'result': result})

def ini_tokoh_detail(request, username_pengguna, nama):
    cursor = connection.cursor()
    result = []
    try:
        cursor.execute("SET SEARCH_PATH TO THECIMS")
        cursor.execute("SELECT username_pengguna, nama, id_rambut, id_mata, id_rumah, warna_kulit, pekerjaan FROM TOKOH WHERE username_pengguna = '{}' AND nama = '{}'".format(username_pengguna,nama))
        
        result= namedtuplefetchall(cursor)
    except Exception as e:
        print(e)
    finally:
        cursor.close()

    return render(request, 'tokohdetail.html', {'result': result})
