from django.urls import path

from . import views

app_name = 'tokoh'

urlpatterns = [
    path('tokoh', views.ini_tokoh, name='tokoh'),
    path('tokoh/<str:username_pengguna>/<str:nama>', views.ini_tokoh_detail, name='tokoh_detail'),
]