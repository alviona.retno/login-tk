from django.http.response import HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import render
from django.db import connection
from collections import namedtuple


# Function to return every row of data from query
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def ini_mengunakan_apparel_admin(request):
    cursor = connection.cursor()
    result = []
    try:
        cursor.execute("SET SEARCH_PATH TO THECIMS")
        cursor.execute("SELECT MA.username_pengguna, MA.nama_tokoh, KJB.nama, A.warna_apparel, A.nama_pekerjaan, A.kategori_apparel FROM KOLEKSI_JUAL_BELI KJB INNER JOIN APPAREL A ON KJB.id_koleksi=A.id_koleksi INNER JOIN MENGGUNAKAN_APPAREL MA ON A.id_koleksi=MA.id_koleksi")
        result= namedtuplefetchall(cursor)
    except Exception as e:
        print(e)
    finally:
        cursor.close()

    return render(request, 'A_menggunakan_apparel.html', {'result': result})

def ini_mengunakan_apparel_pemain(request):
    cursor = connection.cursor()
    result = []
    try:
        cursor.execute("SET SEARCH_PATH TO THECIMS")
        cursor.execute("SELECT MA.username_pengguna, MA.nama_tokoh, KJB.nama, A.warna_apparel, A.nama_pekerjaan, A.kategori_apparel FROM KOLEKSI_JUAL_BELI KJB INNER JOIN APPAREL A ON KJB.id_koleksi=A.id_koleksi INNER JOIN MENGGUNAKAN_APPAREL MA ON A.id_koleksi=MA.id_koleksi INNER JOIN PEMAIN P ON P.username=MA.username_pengguna")
        result= namedtuplefetchall(cursor)
    except Exception as e:
        print(e)
    finally:
        cursor.close()
    
    return render(request, 'P_menggunakan_apparel.html', {'result': result})

def buat_menggunakan_apparel(request):
    return render(request, 'buat_menggunakan_apparel.html')