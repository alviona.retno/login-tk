from django.urls import path

from . import views

app_name = 'menggunakan_apparel'

urlpatterns = [
    path('A_menggunakan_apparel', views.ini_mengunakan_apparel_admin, name='mengunakan_apparel_admin'),
    path('buat_menggunakan_apparel', views.buat_menggunakan_apparel, name='buat_menggunakan_apparel'),
    path('P_menggunakan_apparel', views.ini_mengunakan_apparel_pemain, name='mengunakan_apparel_pemain'),
]