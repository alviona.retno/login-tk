from django.urls import path

from . import views

app_name = 'warnakulit'

urlpatterns = [
    path('A_warnakulit', views.ini_warnakulit_admin, name='warna_kulit_admin'),
    path('P_warnakulit', views.ini_warnakulit_pemain, name='warna_kulit_pemain'),
    path('buat_warnakulit', views.buat_warnakulit, name='buat_warna_kulit')
]