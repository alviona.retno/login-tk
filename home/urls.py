from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('login', views.login, name='login'),
    path('Homepage_pemain', views.loggedInView, name='Homepage_pemain'),
    path('logout', views.logout, name='logout'),
    path('register', views.register, name='register'),
    path('homepage_admin', views.Homepage_admin, name='Homepage_admin'),
]