from django.http.response import HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from .forms import *

# Create your views here.

# Function to return every row of data from query
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def index(request):
    cursor = connection.cursor()
    result = []
    try:
        cursor.execute("SET SEARCH_PATH TO THECIMS")
        cursor.execute("SELECT USERNAME FROM PEMAIN")
        result = namedtuplefetchall(cursor)
    except Exception as e:
        print(e)
    finally:
        cursor.close()

    return render(request, 'index.html', {'result': result})

def login(request):

    result = []

    # Define login form
    MyLoginForm = LoginForm(request.POST)

    # Form submission
    if (MyLoginForm.is_valid() and request.method == 'POST'):
        # Get data from form
        username = MyLoginForm.cleaned_data['username']
        password = MyLoginForm.cleaned_data['password']

        # Run SQL QUERY
        try:
            cursor = connection.cursor()
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            cursor.execute("SELECT * FROM PEMAIN AS ADM WHERE ADM.username ='" + username + "' AND ADM.PASSWORD = '" + password + "'")
            result = cursor.fetchone()

            if(result == None): 
                return HttpResponseNotFound("The user does not exist")

            # Redirect the cursor towards public so it can access Django basic features
            cursor.execute("SET SEARCH_PATH TO public")
            request.session['username'] = [username, password, result]

        except Exception as e:
            print(e)
            cursor.close()

        finally:
            # Don't forget to close
            cursor.close()

        return HttpResponseRedirect('/Homepage_pemain')

    else:
        MyLoginForm = LoginForm()

    return render(request, 'login.html', {'form' : MyLoginForm})

# Function to test logged in result
def loggedInView(request):
   if request.session.has_key('username'):
        cursor = connection.cursor()
        result = []
        try:
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            cursor.execute("SELECT * FROM PEMAIN WHERE USERNAME = '"+ request.session['username'][0] +"'")
            result = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()

        return render(request, 'homepage_pemain.html', {"result" : result})
   else:
        return HttpResponseRedirect('/login')

# Function to log out the user
def logout(request):
   try:
        del request.session['username']
   except:
        pass
   return HttpResponseRedirect('/login')

def register(request):
    return render(request, 'register.html')

def Homepage_admin(request):
    return render(request, 'homepage_admin.html')