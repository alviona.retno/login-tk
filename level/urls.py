from django.urls import path

from . import views

app_name = 'level'

urlpatterns = [
    path('A_level', views.ini_level_admin, name='level_admin'),
    path('P_level', views.ini_level_pemain, name='level_pemain'),
    path('buat_level', views.buat_level, name='buat_level'),
    path('ubah_level', views.ubah_level, name='ubah_level')
]