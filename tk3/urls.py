from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('home.urls')),
    path('', include('tokoh.urls')),
    path('Homepage_pemain/', include('warnakulit.urls')),
    path('Homepage_admin/', include('warnakulit.urls')),
    path('Homepage_pemain/', include('level.urls')),
    path('Homepage_admin/', include('level.urls')),
    path('Homepage_pemain/', include('menggunakan_apparel.urls')),
    path('Homepage_admin/', include('menggunakan_apparel.urls')),
    path('Homepage_pemain/', include('kategori_apparel.urls')),
    path('Homepage_admin/', include('kategori_apparel.urls')),
]